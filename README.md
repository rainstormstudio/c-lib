# C-lib

A helper library for plain C

## Usage

## Roadmap
- [X] default types
- [X] convenient macros
- [ ] memory
- [X] vector
- [ ] stack
- [X] list
- [ ] map
- [X] string
- [ ] debug log
- [ ] smart pointer
- [ ] string format
- [ ] printf
