#ifndef __CLIB_MEMORY_H__
#define __CLIB_MEMORY_H__

#include "clib_types.h"

void *clib_malloc(usize size);
void *clib_realloc(void *ptr, usize size);
void clib_free(void *ptr);

static inline void *clib_memset(void *dest, i32 item, usize n) {
  u8 *ptr = (u8 *)dest;
  for (usize i = 0; i < n; i++) {
    ptr[i] = (u8)item;
  }
  return dest;
}

static inline void clib_memcpy(void *dest, void *src, usize size) {
  u8 *d = (u8 *)dest;
  u8 *s = (u8 *)src;
  for (usize i = 0; i < size; i++) {
    d[i] = s[i];
  }
}

#define NEW(type) (type *)clib_malloc(sizeof(type))
#define NEW_N(type, n) (type *)clib_malloc(sizeof(type) * n)
#define DELETE(ptr) clib_free(ptr)
#define DELETE_N(ptr) clib_free(ptr)

#define MEMSET clib_memset
#define MEMCPY clib_memcpy

#endif
