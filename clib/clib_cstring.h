#ifndef __CLIB_CSTRING_H__
#define __CLIB_CSTRING_H__

#include "clib_types.h"
#include "clib_macros.h"

typedef char* cstring;

static inline usize cstring_len(const cstring str) {
  usize len = 0;
  while (str[len] != 0) {
    len += 1;
  }
  return len;
}

static inline void cstring_add(cstring str, char ch) {
  usize len = cstring_len(str);
  str[len] = ch;
  str[len + 1] = 0;
}

static inline void cstring_append(cstring str, const cstring other) {
  usize len = cstring_len(str);
  for (usize i = 0; other[i] != 0; i ++) {
    str[len] = other[i];
    len += 1;
  }
  str[len] = 0;
}

static inline bool cstring_equal(const cstring str, const cstring other) {
  usize i = 0;
  loop {
    if (str[i] == 0 && other[i] == 0) {
      break;
    }
    if (str[i] == 0 && other[i] != 0) {
      return false;
    }
    if (str[i] != 0 && other[i] == 0) {
      return false;
    }
    if (str[i] != other[i]) {
      return false;
    }
    i += 1;
  }
  return true;
}

#endif
