#ifndef __CLIB_MACROS_H__
#define __CLIB_MACROS_H__

#define STRUCT(name)                                                           \
  typedef struct _##name name;                                                 \
  struct _##name

#define UNION(name)                                                            \
  typedef union _##name name;                                                  \
  union _##name

#define match switch

#define loop while (1)

// TODO move the following to clib_printf
#include <stdio.h>
#define format(str, fmt, ...) sprintf(str, fmt, ##__VA_ARGS__)
#define print(fmt, ...) printf(fmt, ##__VA_ARGS__)
#define println(fmt, ...) printf(fmt "\n", ##__VA_ARGS__)

#endif
