#ifndef __CLIB_STRING_H__
#define __CLIB_STRING_H__

#include "clib_types.h"
#include "clib_macros.h"
#include "clib_memory.h"
#include "clib_cstring.h"

STRUCT(String) {
  usize capacity;
  usize len;
  char* data;
};

static inline void string_init(String* self) {
  self->capacity = 8;
  self->len = 0;
  self->data = NEW_N(char, self->capacity);
  self->data[0] = 0;
}

static inline void string_destroy(String* self) {
  self->capacity = 0;
  self->len = 0;
  DELETE(self->data);
}

static inline void string_add(String* self, char ch) {
  if (self->len == self->capacity - 1) {
    self->capacity = self->capacity * 2;
    self->data = (char*)clib_realloc(self->data, sizeof(char) * self->capacity);
  }
  self->data[self->len] = ch;
  self->len += 1;
  self->data[self->len] = 0;
}

static inline void string_from(String* self, const cstring str) {
  string_init(self);
  usize len = cstring_len(str);
  for (usize i = 0; i < len; i ++) {
    string_add(self, str[i]);
  }
}

static inline void string_append(String* self, const String* other) {
  for (usize i = 0; i < other->len; i ++) {
    string_add(self, other->data[i]);
  }
}

static inline bool string_equal(const String* self, const String* other) {
  return cstring_equal(self->data, other->data);
}

static inline cstring string_c_str(String* self) {
  return self->data;
}

#endif
