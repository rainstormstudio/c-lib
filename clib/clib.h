#ifndef __CLIB_H__
#define __CLIB_H__

#include "clib_cstring.h"
#include "clib_list.h"
#include "clib_macros.h"
#include "clib_memory.h"
#include "clib_string.h"
#include "clib_types.h"
#include "clib_vector.h"

#define define_clib_templates(type)                                            \
  define_vec(type);                                                            \
  define_list(type);                                                           \
                                                                               \
  void EXPECT_SEMICOLON()

#endif
