#ifndef __CLIB_LIST_H__
#define __CLIB_LIST_H__

#include "clib_macros.h"
#include "clib_memory.h"
#include "clib_types.h"

#define define_list(type)                                               \
    STRUCT(list_iter_##type) {                                          \
        type data;                                                      \
        list_iter_##type *prev;                                         \
        list_iter_##type *next;                                         \
    };                                                                  \
                                                                        \
    STRUCT(list_##type) {                                               \
        list_iter_##type *front;                                        \
        list_iter_##type *back;                                         \
        usize size;                                                     \
    };                                                                  \
                                                                        \
    static inline void list_##type##_init(list_##type *self) {          \
        self->size = 0;                                                 \
        self->front = NULL;                                             \
        self->back = NULL;                                              \
    }                                                                   \
                                                                        \
    static inline void list_##type##_destroy(list_##type *self) {       \
        list_iter_##type *cur = self->front;                            \
        while (cur) {                                                   \
            list_iter_##type *tmp = cur;                                \
            cur = cur->next;                                            \
            DELETE(tmp);                                                \
        }                                                               \
    }                                                                   \
                                                                        \
    static inline bool list_##type##_empty(list_##type *self) {         \
        return self->size == 0;                                         \
    }                                                                   \
                                                                        \
    static inline void list_##type##_push_back(list_##type *self, type item) { \
        list_iter_##type *new_iter = NEW(list_iter_##type);             \
        {                                                               \
            new_iter->data = item;                                      \
            new_iter->prev = NULL;                                      \
            new_iter->next = NULL;                                      \
        }                                                               \
        if (self->back) {                                               \
            self->back->next = new_iter;                                \
            new_iter->prev = self->back;                                \
            self->back = self->back->next;                              \
        } else {                                                        \
            self->front = new_iter;                                     \
            self->back = new_iter;                                      \
        }                                                               \
        self->size += 1;                                                \
    }                                                                   \
                                                                        \
    static inline type list_##type##_pop_back(list_##type *self) {      \
        type result;                                                    \
        list_iter_##type *back = self->back;                            \
        result = back->data;                                            \
        if (self->back->prev) {                                         \
            self->back->prev->next = NULL;                              \
            self->back = self->back->prev;                              \
        } else {                                                        \
            self->front = NULL;                                         \
            self->back = NULL;                                          \
        }                                                               \
        DELETE(back);                                                   \
        self->size -= 1;                                                \
        return result;                                                  \
    }                                                                   \
                                                                        \
    static inline void list_##type##_push_front(list_##type *self, type item) { \
        list_iter_##type *new_iter = NEW(list_iter_##type);             \
        {                                                               \
            new_iter->data = item;                                      \
            new_iter->prev = NULL;                                      \
            new_iter->next = NULL;                                      \
        }                                                               \
        if (self->front) {                                              \
            self->front->prev = new_iter;                               \
            new_iter->next = self->front;                               \
            self->front = self->front->prev;                            \
        } else {                                                        \
            self->front = new_iter;                                     \
            self->back = new_iter;                                      \
        }                                                               \
        self->size += 1;                                                \
    }                                                                   \
                                                                        \
    static inline type list_##type##_pop_front(list_##type *self) {     \
        type result;                                                    \
        list_iter_##type *front = self->front;                          \
        result = front->data;                                           \
        if (self->front->next) {                                        \
            self->front->next->prev = NULL;                             \
            self->front = self->front->next;                            \
        } else {                                                        \
            self->front = NULL;                                         \
            self->back = NULL;                                          \
        }                                                               \
        DELETE(front);                                                  \
        self->size -= 1;                                                \
        return result;                                                  \
    }                                                                   \
                                                                        \
    static inline void list_##type##_insert(                            \
        list_##type *self,                                              \
        list_iter_##type *iter, type item) {                            \
        if (iter == NULL) {                                             \
            list_##type##_push_back(self, item);                        \
            return;                                                     \
        }                                                               \
        if (iter == self->front) {                                      \
            list_##type##_push_front(self, item);                       \
            return;                                                     \
        }                                                               \
        list_iter_##type *new_iter = NEW(list_iter_##type);             \
        {                                                               \
            new_iter->data = item;                                      \
            new_iter->prev = NULL;                                      \
            new_iter->next = NULL;                                      \
        }                                                               \
        iter->prev->next = new_iter;                                    \
        new_iter->prev = iter->prev;                                    \
        iter->prev = new_iter;                                          \
        new_iter->next = iter;                                          \
        self->size += 1;                                                \
    }                                                                   \
                                                                        \
    static inline type list_##type##_remove(                            \
        list_##type *self, list_iter_##type *iter) {                    \
        type result;                                                    \
        result = iter->data;                                            \
        if (iter == self->front && iter == self->back) {                \
            self->front = NULL;                                         \
            self->back = NULL;                                          \
        } else if (iter == self->front) {                               \
            self->front = iter->next;                                   \
            self->front->prev = NULL;                                   \
        } else if (iter == self->back) {                                \
            self->back = iter->prev;                                    \
            self->back->next = NULL;                                    \
        } else {                                                        \
            iter->prev->next = iter->next;                              \
            iter->next->prev = iter->prev;                              \
        }                                                               \
        DELETE(iter);                                                   \
        self->size -= 1;                                                \
        return result;                                                  \
    }                                                                   \
                                                                        \
    static inline type *list_##type##_front(list_##type *self) {        \
        if (self->front) {                                              \
            return &self->front->data;                                  \
        }                                                               \
        return NULL;                                                    \
    }                                                                   \
                                                                        \
    static inline type *list_##type##_back(list_##type *self) {         \
        if (self->back) {                                               \
            return &self->back->data;                                   \
        }                                                               \
        return NULL;                                                    \
    }                                                                   \
                                                                        \
    static inline type *list_##type##_at(list_##type *self, usize idx) { \
        if (idx >= self->size) {                                        \
            return NULL;                                                \
        }                                                               \
        list_iter_##type *cur = self->front;                            \
        for (usize i = 0; i < self->size; i++) {                        \
            if (i == idx) {                                             \
                return &cur->data;                                      \
            }                                                           \
            cur = cur->next;                                            \
        }                                                               \
        return NULL;                                                    \
    }                                                                   \
                                                                        \
    void EXPECT_SEMICOLON()

#define ListIter(type) list_iter_##type
#define List(type) list_##type
#define list_init(type) list_##type##_init
#define list_destroy(type) list_##type##_destroy
#define list_empty(type) list_##type##_empty
#define list_push_back(type) list_##type##_push_back
#define list_pop_back(type) list_##type##_pop_back
#define list_push_front(type) list_##type##_push_front
#define list_pop_front(type) list_##type##_pop_front
#define list_insert(type) list_##type##_insert
#define list_remove(type) list_##type##_remove
#define list_front(type) list_##type##_front
#define list_back(type) list_##type##_back
#define list_at(type) list_##type##_at

define_list(i8);
define_list(i16);
define_list(i32);
define_list(i64);

define_list(u8);
define_list(u16);
define_list(u32);
define_list(u64);

define_list(f32);
define_list(f64);

define_list(usize);

#endif
