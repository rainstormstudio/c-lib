#ifndef __CLIB_TYPES_H__
#define __CLIB_TYPES_H__

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

// signed integer
typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

// unsigned integer
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

// floats
typedef float f32;
typedef double f64;

// word
typedef size_t usize;

#endif
