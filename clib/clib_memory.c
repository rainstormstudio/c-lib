#include "clib_memory.h"

// TODO replace malloc/realloc/free with self-written allocator
#include <stdlib.h>

void *clib_malloc(usize size) { return malloc(size); }

void *clib_realloc(void *ptr, usize size) { return realloc(ptr, size); }

void clib_free(void *ptr) { free(ptr); }
