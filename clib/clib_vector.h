#ifndef __CLIB_VECTOR_H__
#define __CLIB_VECTOR_H__

#include "clib_macros.h"
#include "clib_memory.h"
#include "clib_types.h"

#define define_vec(type)                                                       \
  STRUCT(vector_##type) {                                                      \
    usize capacity;                                                            \
    usize size;                                                                \
    type *data;                                                                \
  };                                                                           \
                                                                               \
  static inline void vector_##type##_init(vector_##type *self) {               \
    self->capacity = 4;                                                        \
    self->size = 0;                                                            \
    self->data = NEW_N(type, self->capacity);                                  \
  }                                                                            \
                                                                               \
  static inline void vector_##type##_destroy(vector_##type *self) {            \
    self->capacity = 0;                                                        \
    self->size = 0;                                                            \
    DELETE(self->data);                                                        \
  }                                                                            \
                                                                               \
  static inline void vector_##type##_push(vector_##type *self, type item) {    \
    if (self->size == self->capacity) {                                        \
      self->capacity = self->capacity * 2;                                     \
      self->data =                                                             \
          (type *)clib_realloc(self->data, sizeof(type) * self->capacity);     \
    }                                                                          \
    self->data[self->size] = item;                                             \
    self->size += 1;                                                           \
  }                                                                            \
                                                                               \
  static inline type vector_##type##_pop(vector_##type *self) {                \
    self->size -= 1;                                                           \
    type result = self->data[self->size];                                      \
    return result;                                                             \
  }                                                                            \
                                                                               \
  static inline bool vector_##type##_empty(vector_##type *self) {              \
    return self->size == 0;                                                    \
  }                                                                            \
                                                                               \
  static inline type *vector_##type##_at(vector_##type *self, usize idx) {     \
    return &self->data[idx];                                                   \
  }                                                                            \
                                                                               \
  void EXPECT_SEMICOLON()

#define Vec(type) vector_##type
#define vec_init(type) vector_##type##_init
#define vec_destroy(type) vector_##type##_destroy
#define vec_push(type) vector_##type##_push
#define vec_pop(type) vector_##type##_pop
#define vec_empty(type) vector_##type##_empty
#define vec_at(type) vector_##type##_at

define_vec(i8);
define_vec(i16);
define_vec(i32);
define_vec(i64);

define_vec(u8);
define_vec(u16);
define_vec(u32);
define_vec(u64);

define_vec(f32);
define_vec(f64);

define_vec(usize);

#endif
